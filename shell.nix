# nix-shell
{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
    nativeBuildInputs = [ pkgs.buildPackages.python3 pkgs.buildPackages.sqlite-interactive pkgs.buildPackages ];
    }