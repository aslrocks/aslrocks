- asl.rocks / asl.guide website(s) ISO-639-3 code: ase
  upstream as gitlab@asl.rocks user: https://gitlab.com/aslrocks/aslrocks

  poetry run python -m aslrocks.main

# deployed on OVH

on git push, it should auto-update the deployment on ovh.

How it's deployed:

systemctl edit aslrocks-hyperdiv.service --full --force
As part of deployment, we setup auto-update, via gitlab-ci, curl, caddy, systemd, etc. see main.py's update_self() for docs

- TODO:

  - template
  - ensure et-book fonts are loading from tufte-css.
  - Handshapes:
    Maybe should be a table instead.
    ** sources: \*** https://en.wiktionary.org/wiki/Appendix:Sign_language_handshapes
    **_ https://www.handspeak.com/ref/handshape/index.php
    _** https://www.handspeak.com/study/index.php?id=144

- IDEAS
  ** saner UI for lifeprint.com content
  ** ASL database:
  - store information about the ASL language, like the handshapes and motions for each sign, and the history of signs the possible origins, etc.
  - I know some academic(s) sort of have something like this, but a crowdsourced version that is OSS, could be very useful.
