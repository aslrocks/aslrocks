import subprocess
import hyperdiv as hd

router = hd.router()

# Define the pages of the app:


@router.route('/A2BA14E5-81A9-4178-89E5-FA3DEE50AC18')
def update_self():
    """
    This only works if you can build a ws connection.
    Since we can't, instead we use Caddy's https://github.com/abiosoft/caddy-exec config
    we install it as root: caddy add-package github.com/abiosoft/caddy-exec
    we setup systemd to auto git fetch on restart of the service
    setup sudo in /etc/sudoers.d/caddy-sudoers to allow restart of the service.
    then in Caddyfile, setup exec.
    """
    out = subprocess.run('git pull', shell=True, capture_output=True)
    hd.markdown(out)

@router.route('/media')
def media():
    m = """
There isn't always a lot of media available in ASL, this page is here to
hopefully keep track of what is available.

### ASL streaming services:

 * [vsynplus](https://vsynplus.com)

### Movies

 * Barbie with ASL captioning on [Max](https://www.max.com)
 * Antman with ASL captioning on [Disney+](https://www.disneyplus.com)
 * CODA movie on [Apple TV](https://tv.apple.com/us/movie/coda/umc.cmc.3eh9r5iz32ggdm4ccvw5igiir)

### TV Shows

 * Switched At Birth (About deaf characters, played by deaf actors, but the ASL is not always visible/accurate)
 * Deaf U on [Netflix](https://www.netflix.com/title/81035566) Reality show on Gallaudet campus
 * Echo on [Disney+](https://www.disneyplus.com)

### Youtube

 * [The Daily Moth]()

### Tech for accessibility

 * [Sign up](https://chromewebstore.google.com/detail/signup-sign-language-for/gbllbjbhbafgdcolenjhdoabdjjbjoom) Chrome extension for ASL captioning for Netflix and Disney+

If you have updates for this page, please send them via email to media@asl.rocks

    """
    hd.markdown(m)

@router.route('/')
def home():
	m = """
# ASL Rocks!
This site is dedicated to ASL, and how much it rocks :)

So far not a lot is here, but we are working on that.
If you have things you want to see here or updates, see the contact link in the header.

    """
	hd.markdown(m)


@router.route('/legal')
def legal():
	m = """
# Legal

Various legal information, licensing, etc.
Services are open to the general public, except administration services such as SSH, which are only open
to authorized people.

## No warranties expressed or implied

 * We make no warranties that anything here will be useful or correct.</li>
 * We also make no warranty that anything here will be available past the moment you retrieved it.</li>
 * All services are delivered on a best-effort basis, when the administrators are not busy sleeping or distracted by flowers.</LI>

## Law Enforcement Information

If you are law enforcement, we require subpeonas for information, about a visitor.
However, you should know that we probably do not have any information of use to your investigation(s).

See the header on every page for contact information.

### Information we collect:

 * IP address while a user is currently connected. Usually not logged past 24hrs.
 * Other information as part of the the HTTP protocol(<a href="https://tools.ietf.org/html/rfc2616">RFC 2616</a> and friends). Usually not logged past 24hrs.
 * We do store banned IP addresses for longer than 24 hours(sometimes).
 * We have no analytics collection here.

Being banned usually happens when you try to access private services without proper access.
General browsing of this website will not get you banned.

## Copyrights, Licenses, etc.

This website is copyright 2024 asl.rocks.
Logo from [pngall.com](https://www.pngall.com/de/heavy-metal-png/download/86616) licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)


    """
	hd.markdown(m)


@router.route('/cpr')
def cpr():
	m = """
# You can't make someone more dead from CPR.

Effective [CPR](https://en.wikipedia.org/wiki/Cardiopulmonary_resuscitation)  more than doubles the chance of someone surviving a cardiac arrest. [0](https://www.resus.org.uk/home/faqs/faqs-basic-life-support-cpr).

## How to do Hands-only CPR

When somebody collapses in front of you, what do you do?

 * Check the person over. If they are not responsive and not breathing(and not choking), then their heart has stopped working and they are having a cardiac arrest.
 * Now, call 911. Then you do CPR.
 * If others are around, have them try to find an AED nearby and use it.
 * Lock your fingers together, knuckles up. Then push down, right on the sovereign. Push down five or six centimetres. That&rsquo;s about two inches. Push hard and fast about two times a second, like to the beat of Stayin&rsquo; Alive. Don&rsquo;t worry about hurting someone. A cracked rib can be mended &ndash; just concentrate on saving a life.
 * Keep this up until the ambulance arrives.

 So don't forget. Check them over. Call 911. Push hard and fast to a [fast song](https://open.spotify.com/user/americanheartassociation/playlist/6larteGNSuXdBhaYKLybwa). It works.

In many countries, including the USA, there is a [Good Samaritan Law](https://en.wikipedia.org/wiki/Good_Samaritan_law) that generally prevents you from lawsuits for doing your best to help someone.

Video is from the [Arizona School for the Deaf and Blind.](https://asdb.az.gov/)
Video is in ASL and English. It's 71MB large and might take a little bit to load.
If you have trouble, it's also available on [youtube](https://www.youtube.com/watch?v=eAWg149XCs4).
    """
	hd.markdown(m)
	video = hd.video('/assets/asl-cpr.mp4', volume=0.3)


def hdweb():
	# logo CC BY-NC 4.0 DEED https://www.pngall.com/de/heavy-metal-png/download/86616
	template = hd.template(logo='/assets/aslrocks-logo.jpg', title='ASL Rocks!')

	# Sidebar menu linking to the app's pages:
	template.add_sidebar_menu(
		{
			'Home': {'icon': 'house', 'href': home.path},
			'CPR': {'icon': 'heart-pulse', 'href': cpr.path},
			'Media': {'icon': 'camera-reels', 'href': media.path},
			# icon journals maybe too?
			'Legal': {'icon': 'radioactive', 'href': legal.path},
		}
	)

	# A topbar contact link:
	template.add_topbar_links({'Contact': {'icon': 'envelope', 'href': 'mailto:hello@asl.rocks'}})

	# Render the active page in the body:
	with template.body:
		router.run()


def main():
	hd.run(hdweb)


if __name__ == '__main__':
	main()
